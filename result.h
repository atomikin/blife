/*
 * cmd_result.h
 *
 *  Created on: Sep 27, 2020
 *      Author: atomikin
 */

#ifndef RESULT_H_
#define RESULT_H_
#include <iostream>
#include <memory>
#include <regex>
#include <vector>
#include <map>
#include <chrono>

#include "colour.h"
#include "utils.h"
#include "weather.h"

namespace app {
enum class PowerType
{
	BATTERY,
	AC,
};


enum class BatteryState
{
	CHARGING,
	DISCHARGING
};

enum class Mode
{
	BATTERY,
	WEATHER,
};
/**
 *
drizzle — морось.
light-rain — небольшой дождь.
rain — дождь.
moderate-rain — умеренно сильный дождь.
heavy-rain — сильный дождь.
continuous-heavy-rain — длительный сильный дождь.
showers — ливень.
wet-snow — дождь со снегом.
light-snow — небольшой снег.
snow — снег.
snow-showers — снегопад.
hail — град.
thunderstorm — гроза.
thunderstorm-with-rain — дождь с грозой.
thunderstorm-with-hail — гроза с градом.
 */
enum class WeatherCondition {
	drizzle,
	lightRain,
	rain,
	moderateRain,
	heavyRain,
	continuousHeavyRain,
	showers,
	wetSnow,
	lightSnow,
	snow,
	snowShowers,
	hail,
	thunderstorm,
	thunderstormWithRain,
	thunderstormWithHail,
	cloudy,
	unknown,
};
static const std::map<const std::string, WeatherCondition> stringToCondition = {
	{"drizzle", WeatherCondition::drizzle},
	{"light-rain", WeatherCondition::lightRain},
	{"rain", WeatherCondition::rain},
	{"moderate-rain", WeatherCondition::moderateRain},
	{"heavy-rain", WeatherCondition::heavyRain},
	{"continuous-heavy-rain", WeatherCondition::continuousHeavyRain},
	{"showers", WeatherCondition::showers},
	{"wet-snow", WeatherCondition::wetSnow},
	{"light-snow", WeatherCondition::lightSnow},
	{"snow", WeatherCondition::snow},
	{"snow-showers", WeatherCondition::snowShowers},
	{"hail", WeatherCondition::hail},
	{"thunderstorm", WeatherCondition::thunderstorm},
	{"thunderstorm-with-rain", WeatherCondition::thunderstormWithRain},
	{"thunderstorm-with-hail", WeatherCondition::thunderstormWithHail},
	{"cloudy", WeatherCondition::cloudy},
};

WeatherCondition WeatherConditionFromString(const std::string& rawData);

class IResult
{
public:
	virtual std::string ToPLStatus(const std::string&, const std::string&) const = 0;
//	virtual ~IResult();
};

std::shared_ptr<IResult> ReadResult(Mode, std::istream&);
std::shared_ptr<IResult> RunCommand(Mode, std::string);
std::string GetTmuxStyleTag(const std::string& fg, const std::string& bg);

std::shared_ptr<IResult> LoadBlife(std::istream& input);

Mode GetMode(std::string&);


class BlifeResult : public IResult
{
	PowerType power_type;
	double percents;
	BatteryState state;
	std::string time_remained;
public:
	BlifeResult(PowerType type, double percents, BatteryState state, std::string time_left)
		: percents(percents), state(state), time_remained(time_left), power_type(type)
	{}

	std::string ToPLStatus(const std::string& fg, const std::string& bg) const override;

private:
	Colour GetColour() const;
	const std::string GetIcon() const;
};


class WeatherResult : public IResult, public json::JSONSerializable {
public:
	int temperature;
	WeatherCondition condition;
	int ts;

	WeatherResult():
		temperature(0), condition(WeatherCondition::continuousHeavyRain),
		ts(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count()){}
	virtual ~WeatherResult(){}

	virtual void load(std::istream&) override;
	virtual void dump(std::ostream&) const override;
	virtual std::string ToPLStatus(const std::string& fg, const std::string& bg) const override;
private:
	std::string GetIcon() const;
	std::string FormatTemp() const;
};


} // app


#endif /* RESULT_H_ */
