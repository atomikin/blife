/*
 * main.cpp
 *
 *  Created on: Sep 27, 2020
 *      Author: atomikin
 */
#include "result.h"
#include <iostream>
#include <sstream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <memory>

#include "utils.h"
#include "weather.h"
namespace po = boost::program_options;
namespace fs = boost::filesystem;


int main(int ac, char* av[])
{
	// set up cli interface
	po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
    	("foreground,fg", po::value<std::string>()->default_value("colour16"), "Foreground color")
		("background,bg", po::value<std::string>()->default_value("colour252"), "Background color")
		("config,c", po::value<std::string>()->default_value("~/.tmuxutil/weather.conf", "Weather configuration file"))
		("cmd", po::value<std::string>(), "mode")
		;
    po::positional_options_description pos;
    pos.add("cmd", 1);
    po::variables_map vm;
    po::store(
		po::command_line_parser(ac, av).options(desc).positional(pos).run(),
		vm
	);
    po::notify(vm);
    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }
    if (!vm.count("cmd"))
    {
    	std::cerr << "Mode is a required param" << std::endl;
    	return 1;
    }
    // run app app

    const std::string fg = vm.at("foreground").as<std::string>();
    const std::string bg = vm.at("background").as<std::string>();
    const std::string config = app::utils::Resolve(vm.at("config").as<std::string>()).string();
    try
    {
    	app::Mode mode = app::GetMode(vm.at("cmd").as<std::string>());
//    	std::cout << app::ReadResult(mode, std::cin)->ToPLStatus(fg, bg) << std::endl;
    	std::cout << app::RunCommand(mode, config)->ToPLStatus(fg, bg) << std::endl;
    }
    catch(std::invalid_argument err)
	{
    	std::cerr << err.what() << std::endl;
    	return 1;
	}
	return 0;
}
