/*
 * colour.h
 *
 *  Created on: Sep 27, 2020
 *      Author: atomikin
 */

#ifndef COLOUR_H_
#define COLOUR_H_
#include <string>

namespace app {
enum class Colour
{
	RED,
	GREEN,
	YELLOW,
};

namespace colors {

namespace term {
const std::string RED = "\u001b[31m";
const std::string YELLOW = "\u001b[33m";
const std::string GREEN = "\u001b[32m";
const std::string NC = "\u001b[0m";
} // term

namespace xterm256 {

const std::string GREEN = "colour28";
const std::string RED = "colour9";
const std::string YELLOW = "colour165";

} // xterm256

std::string WrapText(Colour, const std::string&);
const std::string& GetTmuxColour(Colour);

} // colors

namespace plchars {

	const std::string TRIANGLE_LEFT = "\ue0b2";

} // plchars


}

#endif /* COLOUR_H_ */
