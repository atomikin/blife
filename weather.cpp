/*
 * weather.cpp
 *
 *  Created on: May 22, 2021
 *      Author: atomikin
 */
#include <string>
#include <iostream>
#include <memory>
#include <chrono>

#include "Poco/Net/HTTPSClientSession.h"
//#include "Poco/Net/HTTPSSessionFactory.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/StreamCopier.h"
#include "Poco/URI.h"
#include "Poco/Net/Context.h"

#include "weather.h"
#include "utils.h"

using namespace Poco;

using namespace std;
//using namespace boost;


namespace app
{
namespace yandex_weather
{

Mode GetModeFromString(const string& mode)
{
	if (mode == "test")
	{
		return Mode::test;
	}
	return Mode::weatherOnSite;
}

const string GetStringFromMode(const Mode& mode)
{
	switch (mode)
	{
	case Mode::test:
		return "test";
	case Mode::weatherOnSite:
		return "weather_on_size";
	default:
		throw invalid_argument("Illegal mode");
	}
}


void Config::load(istream& stream)
{
	shared_ptr<boost::property_tree::ptree> propertyTree = json::GetPtreeFromStream(stream);
//	path = propertyTree->get<string>("path", "/");
	mode = GetModeFromString(propertyTree->get<string>("mode", ""));
	maxRequestsPerDay = propertyTree->get<size_t>("max_requests_per_day", 50);
	geoPoint.lat = propertyTree->get<double>("geo_point.lat", 0);
	geoPoint.lon = propertyTree->get<double>("geo_point.lon", 0);
	host = propertyTree->get<string>("host", "127.0.0.1");
	port = propertyTree->get<int>("port", 443);
	api_key = propertyTree->get<string>("api_key");
}

void Config::dump(ostream& stream) const
{
	boost::property_tree::ptree resultTree;
	resultTree.put("host", host);
	resultTree.put("port", port);
	resultTree.put("api_key", api_key);
	resultTree.put("mode", GetStringFromMode(mode));
	resultTree.put("max_requests_per_day", maxRequestsPerDay);
	boost::property_tree::ptree geoData;
	geoData.put("lat", geoPoint.lat);
	geoData.put("lon", geoPoint.lon);
	resultTree.add_child("geo_data", geoData);
	boost::property_tree::write_json(stream, resultTree, true);
}

string GetPath(Mode mode)
{
	return mode == Mode::test ? "/v2/forecast" : "/v2/informers";
}

shared_ptr<json::JSONSerializable> Client::Get(shared_ptr<json::JSONSerializable> result) const
{
	string path = GetPath(config->mode);
	URI uri("https://" + config->host + path);
	uri.addQueryParameter("lat", to_string(config->geoPoint.lat));
	uri.addQueryParameter("lon", to_string(config->geoPoint.lon));
	const Net::Context::Ptr context = new Net::Context(
		Net::Context::CLIENT_USE, "", "", "",
		Net::Context::VERIFY_NONE, 9, false, "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH"
	);
	Net::HTTPSClientSession session(uri.getHost(), uri.getPort(), context);
	Net::HTTPRequest request(Net::HTTPRequest::HTTP_GET, uri.getPathAndQuery());
	request.add("X-Yandex-API-Key", config->api_key);
	session.sendRequest(request);
	Net::HTTPResponse response;
	istream& contentStream = session.receiveResponse(response);
	if (response.getStatus() != Net::HTTPResponse::HTTP_OK)
	{
		cout << "\nERROR!";
		throw runtime_error("API retuned error " + to_string(response.getStatus()));
	}
	result->load(contentStream);
	return result;
}

size_t GetTimeFromLastUpdate(size_t requestsPerDay)
{
	return static_cast<size_t>(60 * 60 * 24 / requestsPerDay);
}

bool ShouldUpdate(size_t lastUpdated, size_t requestsPerHour)
{
	size_t now = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	return now - lastUpdated > GetTimeFromLastUpdate(requestsPerHour);
}


} // yandex_weather

} //app
