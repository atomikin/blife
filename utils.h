/*
 * utils.h
 *
 *  Created on: May 30, 2021
 *      Author: atomikin
 */
//#pragma once
#ifndef UTILS_H_
#define UTILS_H_

#include <type_traits>
#include <iostream>
#include <memory>
#include <fstream>
#include <map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>


namespace app
{
namespace json
{

class JSONSerializable {
public:
	virtual void load(std::istream&) = 0;
	virtual void dump(std::ostream&) const = 0;
};


std::shared_ptr<boost::property_tree::ptree> GetPtreeFromStream(std::istream& stream);


template <typename T>
std::shared_ptr<T> LoadJSONFile(const std::string& path)
{
	static_assert(std::is_base_of<JSONSerializable, T>::value, "T must inherit from JSONSerializable");
	auto result = std::make_shared<T>();
	std::ifstream input (path);
	result->load(input);
	input.close();
	return result;
}


void DumpJSONFile(std::shared_ptr<JSONSerializable> data, const std::string& path);

} //json

namespace utils
{

template <typename K, typename V>
V GetWithDef(const  std::map <K,V> & m, const K & key, const V & defval ) {
   typename std::map<K,V>::const_iterator it = m.find( key );
   if ( it == m.end() ) {
      return defval;
   }
   else {
      return it->second;
   }
}

boost::filesystem::path ExpandTilde(const std::string& path);

boost::filesystem::path Resolve(const std::string& path);


}//misc
} // app

#endif /* UTILS_H_ */
