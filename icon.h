/*
 * icon.h
 *
 *  Created on: Sep 27, 2020
 *      Author: atomikin
 */

#ifndef ICON_H_
#define ICON_H_
#include <string>
/*
 * lightning - \xE2\x9A\xA1 U+26A1
 * plug  \xF0\x9F\x94\x8C U+1F50C
 * battery \xF0\x9F\x94\x8B U+1F50B
 */
namespace icons
{
namespace battery
{
const std::string BATTERY = "\xF0\x9F\x94\x8B";
//const std::string PLUG = "\u1f50c";
const std::string PLUG = "\xF0\x9F\x94\x8C";
}// battery
namespace weather
{

const std::string CLOUD_WITH_RAIN = "\xf0\x9f\x8c\xa7";
const std::string SUN = "\xF0\x9F\x8C\x9E";
const std::string CLOUD = "\xE2\x9B\x85";
const std::string CLOUD_WITH_SNOW = "\xF0\x9F\x8C\xA8";
const std::string THUNDERSTORM = "\xE2\x98\x88";

} // weather
}// icons

#endif /* ICON_H_ */
