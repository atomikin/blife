/*
 * utils.cpp
 *
 *  Created on: May 30, 2021
 *      Author: atomikin
 */


#include "utils.h"

namespace fs = boost::filesystem;

namespace app
{
namespace json
{

void DumpJSONFile(std::shared_ptr<JSONSerializable> data, const std::string& path)
{
	std::ofstream output (path);
	data->dump(output);
	output.close();
}

std::shared_ptr<boost::property_tree::ptree> GetPtreeFromStream(std::istream& stream)
{
	boost::property_tree::ptree result;
	boost::property_tree::read_json(stream, result);
	return std::make_shared<boost::property_tree::ptree>(result);
}

} // json

namespace utils
{

fs::path ExpandTilde(const std::string& path)
{
	char const* const home = getenv("HOME");
	if (home == nullptr)
	{
		throw std::invalid_argument("HOME env var is not defined");
	}
	if (!path.empty() && path.find("~/") == 0u)
	{
		return home + path.substr(1);
	}
	return path;
}

fs::path Resolve(const std::string& path)
{
	fs::path expandedPath = ExpandTilde(path);
	return fs::absolute(expandedPath);
}

} //utils
} //app
