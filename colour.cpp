/*
 * colour.cpp
 *
 *  Created on: Sep 28, 2020
 *      Author: atomikin
 */

#include "colour.h"
#include <string>
#include <sstream>

using namespace std;

namespace app {
namespace colors {

string WrapText(Colour cl, const string& line)
{
	ostringstream oss;
	switch(cl) {
	case Colour::RED:
		oss << colors::term::RED;
		break;
	case Colour::GREEN:
		oss << colors::term::GREEN;
		break;
	case Colour::YELLOW:
		oss << colors::term::YELLOW;
		break;
	default:
		throw invalid_argument("Wrong colour type");
	}
	oss << line << colors::term::NC;
	return oss.str();
}

const string& GetTmuxColour(Colour cl)
{
	switch (cl) {
	case Colour::RED:
		return colors::xterm256::RED;
	case Colour::GREEN:
		return colors::xterm256::GREEN;
		break;
	case Colour::YELLOW:
		return colors::xterm256::YELLOW;
		break;
	}
}

} // colors
} // app
