#include "result.h"
#include <exception>
#include <string>
#include <sstream>
#include <memory>
#include <boost/process.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem/path.hpp>

#include "colour.h"
#include "icon.h"
#include "weather.h"
#include "utils.h"


using namespace std;
namespace fs = boost::filesystem;

namespace app {

PowerType GetPowerType(const string& raw)
{
	if (raw == "AC")
	{
		return PowerType::AC;
	} else if (raw == "Battery") {
		return PowerType::BATTERY;
	} else {
		throw invalid_argument("Wrong power type: " + raw);
	}
}

BatteryState GetBatteryState(const string& raw) {
	if (raw == "charged" || raw == "charging")
	{
		return BatteryState::CHARGING;
	} else {
		return BatteryState::DISCHARGING;
	}
}

Mode GetMode(string& modeName)
{
	if (modeName == "battery")
	{
		return Mode::BATTERY;
	}
	if (modeName == "weather") {
		return Mode::WEATHER;
	}
	throw invalid_argument("Unknown mode: " + modeName);
}

shared_ptr<IResult> LoadBlife(istream& input)
{
	string type;
	double percents;
	string battery_state;
	string time_left;

	string tmp;
	getline(input, tmp);

	regex reg ("^[^']+ '(AC|Battery) Power'$");
	smatch match_first;
	if (!regex_match(tmp, match_first, reg))
	{
		throw invalid_argument("Wrong line1: " + tmp);
	}
	type = match_first[1];

	reg = regex(
		"^\\s*.* \\(id=(\\d+)\\)\\s+(\\d+)%;\\s*(charged|discharging|charging);\\s*"
		"(\\d+:\\d+|\\(no estimate\\)).*$"
	);
	smatch match_second;
	tmp.clear();
	getline(input, tmp);
	if (!regex_match(tmp, match_second, reg))
	{
		throw invalid_argument("Wrong line2: " + tmp);
	}
	percents = stod(match_second[2].str());
	battery_state = match_second[3].str();
	time_left = match_second[4].str();
	return make_shared<BlifeResult>(GetPowerType(type), percents, GetBatteryState(battery_state), time_left);
}

const string BlifeResult::GetIcon() const
{
	if (power_type == PowerType::BATTERY)
	{
		return icons::battery::BATTERY;
	}
	return icons::battery::PLUG;
}

Colour BlifeResult::GetColour() const
{
	if (percents > 50)
	{
		return Colour::GREEN;
	}
	if (percents > 20)
	{
		return Colour::YELLOW;
	}
	return Colour::RED;
}
///**
// *
//drizzle — морось.
//light-rain — небольшой дождь.
//rain — дождь.
//moderate-rain — умеренно сильный дождь.
//heavy-rain — сильный дождь.
//continuous-heavy-rain — длительный сильный дождь.
//showers — ливень.
//wet-snow — дождь со снегом.
//light-snow — небольшой снег.
//snow — снег.
//snow-showers — снегопад.
//hail — град.
//thunderstorm — гроза.
//thunderstorm-with-rain — дождь с грозой.
//thunderstorm-with-hail — гроза с градом.
// */
//enum class WeatherCondition {
//	drizzle,
//	lightRain,
//	rain,
//	moderateRain,
//	heavyRain,
//	continuousHeavyRain,
//	showers,
//	wetSnow,
//	lightSnow,
//	snow,
//	snowShowers,
//	hail,
//	thunderstorm,
//	thunderstormWithRain,
//	thunderstormWithHail,
//};

WeatherCondition WeatherConditionFromString(const std::string& rawData)
{
	if(stringToCondition.count(rawData))
	{
		return stringToCondition.at(rawData);
	}
	return WeatherCondition::unknown;
}

string WeatherConditionToString(const WeatherCondition& condition)
{
	// let's build it here for now...
	map<WeatherCondition, string> reverse;
	for(const auto& item : stringToCondition)
	{
		reverse[item.second] = item.first;
	}
	return reverse[condition];
}


string GetTmuxStyleTag(const string& fg, const string& bg)
{
	return "#[fg=" + fg + ",bg=" + bg+ ",bold,noitalics,nounderscore]";
}

string BlifeResult::ToPLStatus(const string& fg, const string& bg) const
{
	ostringstream oss;
	oss << GetTmuxStyleTag(bg, "default");
	oss << plchars::TRIANGLE_LEFT;
	oss << GetTmuxStyleTag(colors::GetTmuxColour(GetColour()), bg);
	oss << GetIcon() << " " << percents << '%';
	if (time_remained != "(no estimate)" && time_remained != "0:00")
	{
		oss<< " (" << time_remained << ")";
	}
	return oss.str();
}

shared_ptr<IResult> ReadResult(Mode mode, istream& input)
{
	switch(mode)
	{
	case Mode::BATTERY:
		return LoadBlife(input);
	default:
		throw invalid_argument("Wrong type");
	}
}

void WeatherResult::load(istream& input)
{
	auto tree = json::GetPtreeFromStream(input);
	auto ptree = *tree;
	if (ptree.count("fact") > 0) {
		ptree = ptree.get_child("fact");
	}
	ts = ptree.get<int>("ts", ts);
	condition = WeatherConditionFromString(ptree.get<string>("condition"));
	temperature = ptree.get<int>("temp");
}


void WeatherResult::dump(ostream& stream) const
{
	boost::property_tree::ptree resultTree;
	resultTree.put("ts", ts);
	resultTree.put("condition", WeatherConditionToString(condition));
	resultTree.put("temp", temperature);
	boost::property_tree::write_json(stream, resultTree, true);
}

string WeatherResult::GetIcon() const
{
	switch (condition){
	case WeatherCondition::drizzle:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::lightRain:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::rain:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::moderateRain:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::heavyRain:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::continuousHeavyRain:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::showers:
		return icons::weather::CLOUD_WITH_RAIN;
		break;
	case WeatherCondition::wetSnow:
		return icons::weather::CLOUD_WITH_SNOW;
		break;
	case WeatherCondition::lightSnow:
		return icons::weather::CLOUD_WITH_SNOW;
		break;
	case WeatherCondition::snow:
		return icons::weather::CLOUD_WITH_SNOW;
		break;
	case WeatherCondition::snowShowers:
		return icons::weather::CLOUD_WITH_SNOW;
		break;
	case WeatherCondition::hail:
		return icons::weather::CLOUD_WITH_SNOW;
		break;
	case WeatherCondition::thunderstorm:
		return icons::weather::THUNDERSTORM;
		break;
	case WeatherCondition::thunderstormWithRain:
		return icons::weather::THUNDERSTORM;
		break;
	case WeatherCondition::thunderstormWithHail:
		return icons::weather::THUNDERSTORM;
		break;
	case WeatherCondition::cloudy:
		return icons::weather::CLOUD;
		break;
	default:
		return icons::weather::SUN;
		break;
	}
}


string WeatherResult::FormatTemp() const
{
	ostringstream oss;
	if (temperature > 0)
	{
		oss << "+";
	}
	else if (temperature < 0)
	{
		oss << "-";
	}
	oss << to_string(temperature);
	return oss.str();
}


string WeatherResult::ToPLStatus(const string& fg, const string& bg) const
{
	ostringstream oss;
	oss << GetTmuxStyleTag(bg, "default");
	oss << plchars::TRIANGLE_LEFT;
	oss << GetTmuxStyleTag(fg, bg);
	oss << "Yandex.Weather|";
	oss << FormatTemp() << " " << GetIcon();
	return oss.str();
}


shared_ptr<IResult> RunCommand(Mode mode, string configPath = "") {
	shared_ptr<IResult> result;
	boost::process::ipstream stream;
	boost::process::child subprocess;
	switch(mode)
	{
	case Mode::BATTERY:
		subprocess = boost::process::child("pmset -g batt", boost::process::std_out > stream);
		result = LoadBlife(stream);
		subprocess.wait();
		return result;

	case Mode::WEATHER:
		{
			auto config = json::LoadJSONFile<yandex_weather::Config>(configPath);
			if (fs::is_regular_file(config->stateFile))
			{
				auto currentState = json::LoadJSONFile<WeatherResult>(config->stateFile);
				if (!yandex_weather::ShouldUpdate(currentState->ts, config->maxRequestsPerDay))
				{
					return currentState;
				}
			}
			yandex_weather::Client client(config);
			auto result = make_shared<WeatherResult>();
			client.Get(result);
			json::DumpJSONFile(result, config->stateFile);
			return result;
		}
		break;
	default:
		throw invalid_argument("Wrong command");
	}
}


}
