/*
 * weather.h
 *
 *  Created on: May 22, 2021
 *      Author: atomikin
 */

#ifndef WEATHER_H_
#define WEATHER_H_


#include <string>
#include <iostream>
#include <memory>

#include "utils.h"

namespace app
{
namespace yandex_weather
{

enum class Mode
{
	test,
	weatherOnSite,
};



Mode GetNodeFromString(const std::string&);


struct GeoPoint {
	double lat;
	double lon;
};


bool ShouldUpdate(size_t, size_t);


size_t GetTimeFromLastUpdate(size_t requestsPerDay);


class Config : public json::JSONSerializable
{
public:
	Config():
		host("127.0.0.1"), mode(Mode::weatherOnSite), maxRequestsPerDay(50),
		port(443), geoPoint{0, 0},
		stateFile(utils::ExpandTilde("~/.yandex_weather_state.json").string()){}

	std::string host;
	int port;
	Mode mode;
	size_t maxRequestsPerDay;
	GeoPoint geoPoint;
	std::string api_key;
	std::string stateFile;

	virtual void load(std::istream&) override;
	virtual void dump(std::ostream&) const override;

};

class Client
{
public:
	const std::shared_ptr<Config> config;
	Client(const std::shared_ptr<Config> config): config(config){}

	std::shared_ptr<json::JSONSerializable> Get(std::shared_ptr<json::JSONSerializable>) const;
};

} //yandex_weather
} //app

#endif /* WEATHER_H_ */
